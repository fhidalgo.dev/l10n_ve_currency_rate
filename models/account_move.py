from odoo import api, fields, models, _
from odoo.exceptions import UserError


class Move(models.Model):
    _inherit = "account.move"

    os_currency_rate = fields.Float(string='Exchange rate', default=1, digits=(12, 2))

    def _check_balanced(self):
        ''' Assert the move is fully balanced debit = credit.
        An error is raised if it's not the case.
        '''
        moves = self.filtered(lambda move: move.line_ids)
        if not moves:
            return
        debit = 0
        credit = 0
        for line in moves.line_ids:
            debit = debit + round(line.debit, moves[0].company_currency_id.decimal_places)
            credit = credit + round(line.credit, moves[0].company_currency_id.decimal_places)

        self.env['account.move.line'].flush(self.env['account.move.line']._fields)
        self.env['account.move'].flush(['journal_id'])
        self._cr.execute('''
            SELECT line.move_id, ROUND(SUM(line.debit - line.credit), currency.decimal_places)
            FROM account_move_line line
            JOIN account_move move ON move.id = line.move_id
            JOIN account_journal journal ON journal.id = move.journal_id
            JOIN res_company company ON company.id = journal.company_id
            JOIN res_currency currency ON currency.id = company.currency_id
            WHERE line.move_id IN %s
            GROUP BY line.move_id, currency.decimal_places
            HAVING ROUND(SUM(line.debit - line.credit), currency.decimal_places) != 0.0;
        ''', [tuple(self.ids)])

        query_res = self._cr.fetchall()
        sums = 0
        if query_res:
            sums = query_res[0][1]

        diff = self.env['account.move.line']
        if int(sums) > 1:
            return super()._check_balanced()

        if sums != 0:
            if debit > credit:
                diff = self.env['account.move.line'].search([('move_id', '=', self.id),
                                                             ('credit', '>', '0')], limit=1)
                amount = round(diff.credit, moves[0].company_currency_id.decimal_places) + sums
                sql = "UPDATE account_move_line SET credit = " + str(amount) + " WHERE id = " + str(diff.id)
                self._cr.execute(sql)

            else:
                diff = self.env['account.move.line'].search([('move_id', '=', self.id),
                                                             ('debit', '>', '0')], limit=1)
                amount = round(diff.debit, moves[0].company_currency_id.decimal_places) + sums
                sql = "UPDATE account_move_line SET debit = " + str(amount) + " WHERE id = " + str(diff.id)
                self._cr.execute(sql)
        return super()._check_balanced()

    def set_os_currency_rate(self):
        for rec in self:
            if rec.invoice_date:
                rate = rec.env['res.currency.rate'].search([('currency_id', '=', rec.currency_id.id),
                                                            ('name', '=', rec.invoice_date)], limit=1).sorted(lambda x: x.name)
                if rec.currency_id.id != rec.company_currency_id.id:
                    if rate:
                        pass
                    else:
                        raise UserError(_(f"There is no exchange rate for {str(rec.invoice_date)} enter the following path Accounting / Configuration / Accounting / Currencies"))
                if rate:
                    exchange_rate = 1 / rate.rate
                    rec.os_currency_rate = exchange_rate

    @api.constrains('invoice_date', 'currency_id')
    def _check_os_currency_rate(self):
        self.set_os_currency_rate()

    @api.onchange('invoice_date', 'currency_id')
    def _onchange_os_currency_rate(self):
        self.set_os_currency_rate()

    @api.onchange('os_currency_rate')
    def _onchange_custom_rate(self):
        self.update_balance()

    @api.constrains('os_currency_rate')
    def _constrains_custom_rate(self):
        self.update_balance()

    def update_balance(self):
        for rec in self.line_ids:
            if rec.amount_currency > 0:
                if self.currency_id.id == self.company_id.currency_id.id:
                    rec.debit = rec.amount_currency
                    rec.debit_aux = rec.amount_currency / self.os_currency_rate
                else:
                    rec.debit = rec.amount_currency * self.os_currency_rate
                    rec.debit_aux = rec.amount_currency
            elif rec.amount_currency < 0:
                if self.currency_id.id == self.company_id.currency_id.id:
                    rec.credit = (rec.amount_currency) * (-1)
                    rec.credit_aux = (rec.amount_currency / self.os_currency_rate) * (-1)
                else:
                    rec.credit = (rec.amount_currency * self.os_currency_rate) * (-1)
                    rec.credit_aux = (rec.amount_currency) * (-1)
