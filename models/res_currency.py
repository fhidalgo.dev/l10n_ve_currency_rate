from datetime import datetime
from odoo import models, fields, api, _
from bs4 import BeautifulSoup
from pytz import timezone
import requests


class ResCurrencyExchangeRate(models.Model):
    _name = "res.currency.exchange.rate"
    _description = "Exchange Rate"

    name = fields.Many2one('res.currency', string='Money')
    server = fields.Selection([('bcv', 'Banco Central de Venezuela'),
                               ('sunacrip', 'SUNACRIP'),
                               ('dolar_today', 'DolarToday'), ])
    currency_ids = fields.Many2many('res.currency.rate',
                                    compute='_compute_res_currency',
                                    string='Rate')

    def sunacrip(self):
        """Get the dollar rate from sunacrip"""
        headers = {'Content-type': 'application/json'}
        data = '{"coins":["%s"], "fiats":["USD"]}' % (self.name.name)
        response = requests.post('https://petroapp-price.petro.gob.ve/price/',
                                 headers=headers, data=data)
        check = response.json()

        if check['status'] == 200 and check['success']:
            check = float(check['data']['' + self.name.name + '']['USD'])
            return check
        return False

    def central_bank(self):
        """Get the dollar rate from bcv"""
        url = "http://www.bcv.org.ve/"
        content = requests.get(url)

        status_code = content.status_code
        if status_code == 200:
            html = BeautifulSoup(content.text, "html.parser")
            # Euro
            euro = html.find('div', {'id': 'euro'})
            euro = str(euro.find('strong')).split()
            euro = str.replace(euro[1], '.', '')
            euro = float(str.replace(euro, ',', '.'))
            # Dolar
            dolar = html.find('div', {'id': 'dolar'})
            dolar = str(dolar.find('strong')).split()
            dolar = str.replace(dolar[1], '.', '')
            dolar = float(str.replace(dolar, ',', '.'))

            if self.name.name == 'USD':
                return dolar
            if self.name.name == 'EUR':
                return euro
            return False
        return False

    def dtoday(self):
        """Get the dollar rate from dollar today"""
        url = "https://s3.amazonaws.com/dolartoday/data.json"
        response = requests.get(url)
        status_code = response.status_code

        if status_code == 200:
            response = response.json()
            usd = float(response['USD']['transferencia'])
            eur = float(response['EUR']['transferencia'])

            if self.name.name == 'USD':
                return usd
            if self.name.name == 'EUR':
                return eur
            return False
        return False

    def set_rate(self):
        """Set exchange rate"""
        if self.server == 'bcv':
            currency = self.central_bank()
        elif self.server == 'dolar_today':
            currency = self.dtoday()
        elif self.server == 'sunacrip':
            currency = self.sunacrip()
        rate = self.env['res.currency.rate'].search([('name', '=', datetime.now()),
                                                     ('currency_id', '=', self.name.id)])
        if len(rate) == 0:
            self.env['res.currency.rate'].create({
                'currency_id': self.name.id,
                'name': datetime.now(),
                'sell_rate': round(currency, 2),
                'rate': 1 / round(currency, 2)
            })
        else:
            rate.rate = 1 / round(currency, 2)
            rate.sell_rate = round(currency, 2)
        if self.name.id == 2:
            self.update_product(round(currency, 2))

    def update_product(self, currency):
        """Update product prices"""
        product = self.env['product.template'].search([('list_price_usd', '>', 0)])
        for rec in product:
            rec.list_price = rec.list_price_usd * currency
        product_attribute = self.env['product.template.attribute.value'].search([])
        for rec in product_attribute:
            rec.price_extra = rec.list_price_usd * currency

    @api.model
    def _cron_update_product(self):
        exchange = self.env['res.currency.exchange.rate'].search([])
        for rec in exchange:
            rec.set_rate()

    @api.model
    def _compute_res_currency(self):
        for rec in self:
            rate = self.env['res.currency.rate'].search([
                ('currency_id', '=', rec.name.id), ])
            rec.currency_ids = rate


class ResCurrencyRate(models.Model):
    _inherit = 'res.currency.rate'

    sell_rate = fields.Float(string='Exchange rate', digits=(12, 4))

    @api.constrains("sell_rate")
    def set_sell_rate(self):
        """Set the sale rate"""
        self.rate = 1 / self.sell_rate

    def get_systray_dict(self, date):
        tz_name = "America/Caracas"
        today_utc = datetime.strptime(date, '%Y-%m-%dT%H:%M:%S.%fZ')
        context_today = today_utc.astimezone(timezone(tz_name))
        date = context_today.strftime("%Y-%m-%d")
        rate = self.env['res.currency.rate'].search([
            ('currency_id', '=', 2),
            ('name', '=', date)], limit=1).sorted(lambda x: x.name)
        if rate:
            exchange_rate = 1 / rate.rate
            return {'date': _('Date : ') + rate.name.strftime("%d/%m/%Y"), 'rate': "Bs/USD: " + str("{:,.2f}".format(exchange_rate))}
        return {'date': _('No currency rate for ') + context_today.strftime("%d/%m/%Y"), 'rate': 'N/R'}
